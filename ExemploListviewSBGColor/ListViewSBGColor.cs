﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace ExemploListviewSBGColor
{
    public partial class ListViewSBGColor : ListView
    {
        public ListViewSBGColor()
        {
            InitializeComponent();
        }

        public ListViewSBGColor(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        #region Metodos

        public void LinhaZebradaListView()
        {
            for (int i = 0; i < Items.Count; i++)
            {
                if (i % 2 == 0)
                    Items[i].BackColor = ItemBackgroundColor1;
                else
                    Items[i].BackColor = ItemBackgroundColor2;
            }
        }

        private void ListView_SelectedIndexChanged(object sender, EventArgs e)
        {
            var item = FocusedItem;

            if (item == null)
            {
                if (SelectedItems.Count == 0) return;

                SelectedItems[0].Focused = true;
                item = FocusedItem;
            }
            LinhaZebradaListView();

            item.Selected = false;

            if (Items[item.Index].Focused)
            {
                Items[item.Index].BackColor = SelectedItemBGColor;
            }
            else
            {
                Items[item.Index].BackColor = SelectedItemBGColor;
            }
        }

        #endregion

        #region Propriedades

        public Color ItemBackgroundColor1 { get; set; }
        public Color ItemBackgroundColor2 { get; set; }
        public Color SelectedItemBGColor { get; set; }

        #endregion
    }
}
