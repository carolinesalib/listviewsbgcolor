﻿using System;
using System.Windows.Forms;

namespace ExemploListviewSBGColor
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
            listViewSBGColor1.FullRowSelect = true;
            listViewSBGColor1.GridLines = true;
            listViewSBGColor1.View = View.Details;

            listViewSBGColor1.Columns.Add("Coluna1", 150, HorizontalAlignment.Left);
            listViewSBGColor1.Columns.Add("Coluna2", 150, HorizontalAlignment.Left);
            listViewSBGColor1.Columns.Add("Coluna3", 150, HorizontalAlignment.Left);
            listViewSBGColor1.Columns.Add("Coluna4", 150, HorizontalAlignment.Left);
            
            for (int i = 0; i < 10; i++)
            {
                var item = new ListViewItem("Item - " + 1);
                item.SubItems.Add("Item - " + 1);
                item.SubItems.Add("Item - " + 1);
                item.SubItems.Add("Item - " + 1);

                listViewSBGColor1.Items.Add(item);
            }

            listViewSBGColor1.Items[0].Selected = true;
        }
    }
}
