﻿namespace ExemploListviewSBGColor
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.listViewSBGColor1 = new ExemploListviewSBGColor.ListViewSBGColor(this.components);
            this.SuspendLayout();
            // 
            // listViewSBGColor1
            // 
            this.listViewSBGColor1.ItemBackgroundColor1 = System.Drawing.Color.Silver;
            this.listViewSBGColor1.ItemBackgroundColor2 = System.Drawing.Color.White;
            this.listViewSBGColor1.Location = new System.Drawing.Point(58, 44);
            this.listViewSBGColor1.Name = "listViewSBGColor1";
            this.listViewSBGColor1.SelectedItemBGColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.listViewSBGColor1.Size = new System.Drawing.Size(676, 350);
            this.listViewSBGColor1.TabIndex = 0;
            this.listViewSBGColor1.UseCompatibleStateImageBehavior = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(821, 469);
            this.Controls.Add(this.listViewSBGColor1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private ListViewSBGColor listViewSBGColor1;
    }
}

